package com.yuzf.netty.example.nio.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.SocketChannel;

/**
 * @ClassName UserInputHandler
 * @Description: 用户输入处理线程
 * @Author yuzf
 * @Date 2020/6/23
 * @Version V1.0
 **/
public class UserInputHandler implements Runnable{

    private ChatClient client;

    public UserInputHandler(ChatClient client) {
        this.client = client;
    }

    @Override
    public void run() {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true){
                String input = reader.readLine();
                client.send(input);
                if(client.readyToQuit(input)){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
