package com.yuzf.netty.example.nio.filecopy;

import java.io.*;

public class BufferedFileCopy implements FileCopy{
    @Override
    public void copy(File source, File target) {
        try(
                InputStream in = new BufferedInputStream(new FileInputStream(source));
                OutputStream out = new BufferedOutputStream(new FileOutputStream(target))
        ) {
            byte[] buffer = new byte[1024];
            int end;
            while ((end = in.read(buffer))!=-1){
                out.write(buffer,0, end);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BufferedFileCopy bfc = new BufferedFileCopy();
        File source = new File("E:\\demo.txt");
        File target = new File("E:\\test\\copy.txt");
        if(!target.exists()){
            try {
                target.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bfc.copy(source, target);
    }
}
