package com.yuzf.netty.example.nio.filecopy;

import java.io.*;

public class CommonFileCopy implements FileCopy{


    @Override
    public void copy(File source, File target) {
        try (InputStream in = new FileInputStream(source);
             OutputStream out = new FileOutputStream(target)) {
            int end;
            while ((end=in.read())!=-1){
                System.out.println();
                out.write(end);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CommonFileCopy cfc = new CommonFileCopy();
        File source = new File("E:\\demo.txt");
        File target = new File("E:\\test\\copy.txt");
        if(!target.exists()){
            try {
                target.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cfc.copy(source, target);
    }
}
