package com.yuzf.netty.example.nio.filecopy;

import java.io.File;

public interface FileCopy {

    void copy(File source, File target);
}
