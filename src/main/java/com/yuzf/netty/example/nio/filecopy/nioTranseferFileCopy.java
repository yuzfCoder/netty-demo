package com.yuzf.netty.example.nio.filecopy;

import java.io.*;
import java.nio.channels.FileChannel;

public class nioTranseferFileCopy implements FileCopy {
    @Override
    public void copy(File source, File target) {
        try (
                FileChannel in = new FileInputStream(source).getChannel();
                FileChannel out = new FileOutputStream(target).getChannel();
        ) {
            long transfered = 0L;
            while (transfered != in.size()) {
                transfered += in.transferTo(0, in.size(), out);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        nioTranseferFileCopy ntfc = new nioTranseferFileCopy();
        File source = new File("E:\\demo.txt");
        File target = new File("E:\\test\\copy.txt");
        if(!target.exists()){
            try {
                target.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ntfc.copy(source, target);
    }
}
