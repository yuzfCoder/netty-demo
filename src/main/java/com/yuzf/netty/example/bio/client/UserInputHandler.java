package com.yuzf.netty.example.bio.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInputHandler implements Runnable{
    private  Client client;

    public UserInputHandler(Client client) {
        this.client = client;
    }


    @Override
    public void run() {

       try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
           while (true){
               String input = reader.readLine();
               client.send(input);
               if(client.readyToQuit(input)){
                   break;
               }
           }
       } catch (IOException e) {
           e.printStackTrace();
       }
    }


}
